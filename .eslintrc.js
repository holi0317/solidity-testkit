module.exports = {
  root: true,
  extends: ["eslint:recommended"],
  parserOptions: {
    ecmaVersion: "latest",
  },

  env: {
    es6: true,
    node: true,
    mocha: true,
  },

  globals: {
    web3: true,
    artifacts: true,
    contract: true,
  },
};

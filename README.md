# Solidity testing kit

Stater kit for doing solidity testing.

## Installation

1. Make sure your system already got nodejs installed
   - Macos install with `brew install nodejs`
   - Windows install with `scoop install nodejs`
   - Linux you know what to do
2. Install [truffle] with `npm install truffle -g`
3. Run `npm install` on this repository

[truffle]: https://trufflesuite.com/truffle/

## VSCode plugins

- [solidity](https://marketplace.visualstudio.com/items?itemName=JuanBlanco.solidity)
- [eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) (optional but recommended)

## Development console

This is (a bit?) useful for debugging unit tests. This will give you a REPL nodejs console
with access to environment similar to the testing environment.

1. Run `truffle compile` to compile your contracts (Need to do this as the develop won't compile for you automatically)
2. Run `truffle develop` to get the REPL console

Note that you don't need to do `artifacts.require` call in REPL. Contracts will be available in global namespace.

## Writing test

Read `test/TimeMachine.js` for how to write a simple test. Either adapt from this file or copy that
file as a starting point.

## Running test

Run `truffle test`.

If you have multiple test files, you can specify which test file to run by adding the file as the last
argument.

For example, run `truffle test test/TimeMachine.js` to only run tests in `test/TimeMachine.js` file.

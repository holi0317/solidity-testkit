// SPDX-License-Identifier: MIT
pragma solidity >=0.8.8 <0.9.0;

/// Lock ether in the contract for some time before withdrawing.
/// When ether is locked, the accounts can cast a vote
contract TimeMachine {
    /// Time period for locking
    uint256 public constant LOCK_PERIOD = 3 days;

    /// Some vote options
    enum Option {
        Pizza,
        Pasta,
        Burger
    }

    /// Currently collected votes
    mapping(Option => uint256) public votes;

    /// Balance for each account locked in this contract
    mapping(address => uint256) public balance;

    /// Time for locking the ether
    mapping(address => uint256) public lockTime;

    /// If maps to true, the address have casted a vote
    mapping(address => bool) public voted;

    bool internal locked;

    modifier noReentrant() {
        require(!locked, "No re-entrancy");
        locked = true;
        _;
        locked = false;
    }

    /// Deposit some ether into the contract.
    /// If already deposited, this will throw.
    /// If this function is called without a value, this will throw.
    function deposit() public payable {
        require(balance[msg.sender] == 0, "Must have not deposited");
        require(msg.value > 0, "Must pay something");

        balance[msg.sender] = msg.value;
        lockTime[msg.sender] = block.timestamp;
        voted[msg.sender] = false;
    }

    /// Withdraw deposited ether when time matures
    function withdraw() public noReentrant {
        require(balance[msg.sender] > 0, "Must have deposited before");
        require(
            lockTime[msg.sender] + LOCK_PERIOD < block.timestamp,
            "Have not reached unlock time"
        );

        payable(msg.sender).transfer(balance[msg.sender]);
        balance[msg.sender] = 0;
        // No need to reset the lock time
    }

    /// Cast a vote.
    /// Throws if did not have any balance.
    /// Throws if already voted in this deposit cycle.
    function vote(Option option) public {
        require(balance[msg.sender] > 0, "You have no stake in the society");
        require(!voted[msg.sender], "One account one vote");

        votes[option] += 1;
        voted[msg.sender] = true;
    }
}

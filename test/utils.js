/**
 * Utilities for testing.
 */

const { assert } = require("chai");

/**
 * Increase the block time for **next** block.
 *
 * Note that this after calling function, view functions in contract will not update
 * accordingly unless there is a new transaction in the chain.
 *
 * @param {number} amount Amount of time (in seconds) to increase
 * @returns {Promise}
 */
module.exports.increaseTime = function increaseTime(amount) {
  return new Promise((resolve, reject) => {
    web3.currentProvider.send(
      {
        jsonrpc: "2.0",
        method: "evm_increaseTime",
        params: [amount],
        id: 0,
      },
      (err, resp) => {
        if (err != null) {
          reject(err);
          return;
        }

        if (resp.error != null) {
          reject(resp.error);
          return;
        }

        resolve();
      }
    );
  });
};

/**
 * Assert given transaction will throw (or revert, or fail require) with given reason
 *
 * @param {*} transaction Transaction (that has not yet been awaited)
 * @param {string} reason Reason string given to `require` call in contract
 * @param {string?} message Optional message for assert message in testing
 * @returns
 */
module.exports.assertThrow = async function assertThrow(
  transaction,
  reason,
  message
) {
  try {
    await transaction;
  } catch (err) {
    assert.equal(err.reason, reason, message);
    return;
  }

  assert.fail("Should have thrown exception");
};

/**
 * Get the gas usage for given transaction and log out to console.
 *
 * This supports both contract deployment transaction and function call transactions.
 *
 * @param {*} tx Transaction (that has been awaited)
 * @param {string} reason Reason for this transaction, for logging reason only.
 * @returns Gas usage
 */
module.exports.gasUsage = async function gasUsage(tx, reason) {
  const hash = tx.transactionHash ?? tx.tx;
  const receipt = await web3.eth.getTransactionReceipt(hash);
  const gas = receipt.cumulativeGasUsed;

  console.log(`${reason} used gas ${gas}`);

  return gas;
};

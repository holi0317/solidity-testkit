/**
 * Template (and guide) for how to write unit test on a solidity contract.
 */

// ==== Importing modules. Just keep them ====
const { increaseTime, assertThrow, gasUsage } = require("./utils");
const { assert } = require("chai");
// ==== End imports ====

// Get a reference to the contract. You can put any string for left arrow but keeping it the
// same as contract name would be easier to maintain.
// The text in right arrow **must be the contract's name**. Do not put the file name here
// (if your filename is different from contract name).
// ---- ⬇ ----------------------------- ⬇
const TimeMachine = artifacts.require("TimeMachine");

// ⬇ Each contract's test should start with contract call. I suggest only contain 1 contract test per test file to make things easy.
contract("TimeMachine", (accounts) => {
  // ------------------- ⬆ Each test will come with 10 accounts, each with 1000 ETH in it. You can access them by...
  // ----- ⬇ Using this syntax, you can reference accounts by name in the tests. If you need more than 2 accounts, just append variable before `]`
  const [Alice, Bob] = accounts;

  // Each test case should consist:
  // 1. Creating the contract
  // 2. Do some transactions
  // 3. Get the contract's state and blockchain's state (wallet balance). And see if they match the expectation (assert)

  // -- ⬇ The string here should describe this test case. Try read the sentence as "it should accept deposit"
  it("should accept deposit", async () => {
    // ---------------------- ⬆ Just copy and paste the async and don't ask question. If you want to know more, read "async function" on mdn

    // Call the constructor of the contract and deploy it on chain.
    // This constructor does not need any parameter so there's nothing in the ()
    // The owner should be `accounts[0]`. There is a way to define who runs this transaction
    // and will be explained later
    const instance = await TimeMachine.new();
    // -------------- ⬆ IMPORTANT: Remember to add `await` here.
    // All contract function call must include `await`. Otherwise funny things (no they are not) will happen

    // Call methods in the contract as if it is a javascript class (just `.` it)
    await instance.deposit({
      // <- After all parameters specified, surround optional transaction stuff with {}
      from: Alice, // <- Specify which account the transaction came from
      value: web3.utils.toWei("1", "ether"), // <- Specify the value (ether transferred in). Note that the value need to be string quoted.
    });
  });

  it("should reject empty deposit", async () => {
    const instance = await TimeMachine.new();

    // Assert the given transaction revert with some message.
    // See documentation on `assertThrow` for details
    await assertThrow(instance.deposit({ from: Alice }), "Must pay something");
    // ---------------- ⬆ Note there is no `await` here
  });

  it("should allow vote for deposited account", async () => {
    const instance = await TimeMachine.new();

    await instance.deposit({
      from: Bob,
      value: web3.utils.toWei("1", "ether"),
    });

    // Access enum by `.`-ing the contract instead of the contract instance.
    const option = TimeMachine.Option.Pizza;

    // ----------------- ⬇ Pass in contract argument as if position argument
    await instance.vote(option, { from: Bob });
    // -------------------------- ⬆ Pass in transaction options in the end as a javascript object

    // Accessing mapping is a bit weird. They are like a function that accepts the key and return the value
    const actualVote = await instance.votes(option);

    // Use this syntax for asserting. vscode should help you when you type `assert.`
    // See chai's assert documentation for what functions are available.
    // Seriously you should only use `assert.equal` and `assert` most of the time
    // https://www.chaijs.com/api/assert/
    assert.equal(actualVote, 1, "Should have 1 vote for pizza");
  });

  it("should allow withdraw after some time", async () => {
    const instance = await TimeMachine.new();

    await instance.deposit({
      from: Bob,
      value: web3.utils.toWei("1", "ether"),
    });

    // Let's time travel. This will increase the **next block**'s time to the specified time (in seconds)
    await increaseTime(259200 + 10);
    // ---------------- ⬆ Just google "3 days to seconds" to get this number with help of modern technology
    // Remember to add a bit more time to make sure the transaction will reach the future that's distant enough

    // Now we should be able to withdraw. Let's also show how to do assert on change in wallet balance

    // Store the balance pre-withdraw
    const original = await web3.eth.getBalance(Bob);
    await instance.withdraw({ from: Bob });
    const changed = await web3.eth.getBalance(Bob);

    // THE CODE BELOW WILL NOT WORK
    // Remember there's gas fee? Bob should've gained less than 1 ethereum
    // assert.equal(changed - original, web3.utils.toWei("1", "ether"), "Should return my ethereum")

    // Best thing we can do is the diff is around 1 ether
    assert(
      changed - original > web3.utils.toWei("0.9", "ether"),
      "Should have returned my ether"
    );
  });

  // This "tests" the gas usage. But in fact it just collect the gas usage to console
  it("gas usage", async () => {
    const instance = await TimeMachine.new();
    // Pass in the contract instance to get how much gas was used for creating the contract
    await gasUsage(instance, "Contract creation");

    // Wrap a contract function call with `gasUsage` to get it's usage
    // Notice the 2 `await` here. Both `gasUsage` and the transaction need to be `await`ed
    await gasUsage(
      await instance.deposit({
        from: Bob,
        value: web3.utils.toWei("1", "ether"),
      }),
      "Deposit" // <- Reason for the gas usage. For showing in console only
    );

    // This should show the following on console in the end:
    // Contract creation used gas 786095
    // Deposit used gas 68122
  });
});
